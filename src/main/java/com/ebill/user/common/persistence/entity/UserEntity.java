package com.ebill.user.common.persistence.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.Date;

/**
 * Created by santhosh on 7/1/17.
 */

@Document(collection = "users")
public class UserEntity {
    public static final String NAME = "name";
    public static final String PASSWORD = "password";
    public static final String TYPE = "type";
    public static final String P_HINT = "password_hint";
    public static final String UPDATE_TS = "update_ts";
    public static final String CREATE_TS = "create_ts";
    public static final String ADDRESS = "address";
    public static final String LOGO = "logo";
    public static final String GST_NUMBER = "gst_number";
    public static final String NO_OF_SEATING = "no_of_seating";




    @Id
    private String userName;

    @Field(NAME)
    private Name name;

    @Field(PASSWORD)
    private String password;

    @Field(TYPE)
    private String type;

    @Field(P_HINT)
    private String passwordHint;

    @Field(UPDATE_TS)
    private Date updateTimestamp;

    @Field(CREATE_TS)
    private Date createTimestamp;


    @Field(NO_OF_SEATING)
    private int numberOfSeatings;

    @Field(LOGO)
    private String logo;

    @Field(GST_NUMBER)
    private String gstNumber;

    @Field(ADDRESS)
    private String address;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Name getName() {
        return name;
    }

    public void setName(Name name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getUpdateTimestamp() {
        return updateTimestamp;
    }

    public void setUpdateTimestamp(Date updateTimestamp) {
        this.updateTimestamp = updateTimestamp;
    }

    public Date getCreateTimestamp() {
        return createTimestamp;
    }

    public void setCreateTimestamp(Date createTimestamp) {
        this.createTimestamp = createTimestamp;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPasswordHint() {
        return passwordHint;
    }

    public void setPasswordHint(String passwordHint) {
        this.passwordHint = passwordHint;
    }

    public int getNumberOfSeatings() {
        return numberOfSeatings;
    }

    public void setNumberOfSeatings(int numberOfSeatings) {
        this.numberOfSeatings = numberOfSeatings;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getGstNumber() {
        return gstNumber;
    }

    public void setGstNumber(String gstNumber) {
        this.gstNumber = gstNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public class Name {
        private String firstName;
        private String lastName;

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }
    }

}
