package com.ebill.user.common.persistence.repository;

import com.ebill.user.common.persistence.entity.UserEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by santhosh on 7/1/17.
 */
public interface UserSignUpRepository extends MongoRepository<UserEntity, String>{
}
