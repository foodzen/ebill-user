package com.ebill.user.common.authentication;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * Created by santhosh on 7/1/17.
 */
@Service
public class AuthenticationService {


    public String encrypt(String password) {
        try {
            String hashedPassword = DigestUtils.md5Hex(password);
            return hashedPassword;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public boolean checkPassword(String password , String hashedPassword) {
        try {
            return  hashedPassword.equalsIgnoreCase(DigestUtils.md5Hex(password));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

}
