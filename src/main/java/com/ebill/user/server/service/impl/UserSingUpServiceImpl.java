package com.ebill.user.server.service.impl;

import com.ebill.user.common.authentication.AuthenticationService;
import com.ebill.user.common.persistence.entity.UserEntity;
import com.ebill.user.common.persistence.repository.UserSignUpRepository;
import com.ebill.user.server.rest.entity.SignupRequestEntity;
import com.ebill.user.server.service.UserSingUpService;
import com.ebill.user.server.rest.entity.SigninRequestEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by santhosh on 7/1/17.
 */
@Service
public class UserSingUpServiceImpl implements UserSingUpService {

    private final static Logger log = LoggerFactory.getLogger( UserSingUpServiceImpl.class );


    @Autowired
    private UserSignUpRepository userSignUpRepository;

    @Autowired
    private AuthenticationService authenticationService;

    @Override
    public String registerUser(SignupRequestEntity signupRequestEntity) {
        UserEntity userEntity = new UserEntity();
        userEntity.setPassword(authenticationService.encrypt(signupRequestEntity.getPassword()));
        userEntity.setCreateTimestamp(new Date());
        userEntity.setUpdateTimestamp(new Date());
        UserEntity.Name name = userEntity.new Name();
        name.setFirstName(signupRequestEntity.getFirstName());
        name.setLastName(signupRequestEntity.getLastName());
        userEntity.setName(name);
        userEntity.setType(signupRequestEntity.getType());
        userEntity.setPasswordHint(signupRequestEntity.getPasswordHint());
        userEntity.setUserName(signupRequestEntity.getUserName());
        userEntity.setLogo(signupRequestEntity.getLogo());
        userEntity.setGstNumber(signupRequestEntity.getGstNumber());
        userEntity.setNumberOfSeatings(signupRequestEntity.getNumberOfSeatings());
        userEntity.setAddress(signupRequestEntity.getAddress());
        userSignUpRepository.insert(userEntity);
        return signupRequestEntity.getUserName();
    }

    @Override
    public UserEntity loginUser(SigninRequestEntity signinRequestEntity) {
        UserEntity userDocument = userSignUpRepository.findOne(signinRequestEntity.getUserName());
        Map<String,String> response = new HashMap<>();
        if (userDocument == null) {
            log.error("User not available");
            response.put("response","User Not Registered");
            response.put("code","700");
            return userDocument;
        }
        if (authenticationService.checkPassword(signinRequestEntity.getPassword(), userDocument.getPassword())) {
            log.error("User available");
            response.put("response","Sign In Success");
            response.put("code","200");
            response.put("type",userDocument.getType());
            return userDocument;
        } else {
            log.error("login failed");
            response.put("response","Wrong password");
            response.put("code","700");
            return null;
        }
    }
}
