package com.ebill.user.server.service;

import com.ebill.user.common.persistence.entity.UserEntity;
import com.ebill.user.server.rest.entity.SignupRequestEntity;
import com.ebill.user.server.rest.entity.SigninRequestEntity;

/**
 * Created by santhosh on 7/1/17.
 */
public interface UserSingUpService {
    public String registerUser(SignupRequestEntity signupRequestEntity);

    public UserEntity loginUser(SigninRequestEntity signinRequestEntity);

}
