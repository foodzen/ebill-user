package com.ebill.user.server.rest.controller;

import com.ebill.user.common.persistence.entity.UserEntity;
import com.ebill.user.server.rest.entity.SigninRequestEntity;
import com.ebill.user.server.rest.entity.SignupRequestEntity;
import com.ebill.user.server.service.UserSingUpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/*
 * Created by santhosh on 7/1/17.
 */

@RestController
@CrossOrigin
@RequestMapping("/ebill")
public class UserController {

    @Autowired
    private UserSingUpService userSingUpService;

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String registerModule(@Validated @RequestBody SignupRequestEntity signupRequestEntity) {
        return userSingUpService.registerUser(signupRequestEntity);
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public UserEntity loginModule(@Validated @RequestBody SigninRequestEntity signinRequestEntity) {
        return userSingUpService.loginUser(signinRequestEntity);
    }

}
