FROM ubuntu:xenial

MAINTAINER Techops "santhosh2342@outlook.com"

RUN apt-get update && apt-get install -y openjdk-8-jdk && apt-get install -y supervisor

RUN mkdir -p /home/app/installs/ebill-user

RUN mkdir -p /var/log/ebill-user

VOLUME /tmp

ARG JAR_FILE

COPY ${JAR_FILE} /home/app/installs/ebill-user

COPY ${JAR_FILE}/deploy/supervisor/ebill-user.conf /etc/supervisor/conf.d/supervisord.conf

EXPOSE 9000

ENTRYPOINT /usr/bin/supervisord -n