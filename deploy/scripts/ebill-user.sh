#!/usr/bin/env bash

sleep 10s

java -jar /home/app/installs/ebill-user/target/ebill-user-1.0-SNAPSHOT.jar

sleep 300s

echo "Killing supervisord"
ps -ef | grep supervisord | grep -v grep | awk '{ print $2 }' | xargs kill
